.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

=======
Sale UX
=======

Ajustes a modulo de Ventas:

#. Eliminar botón 'enviar por mail'
#. Eliminar botón 'imprimir'
#. Eliminar botón 'previsualizar'
#. Campo 'Equipo de Venta' se muestra en cabecera de pedidos
#. Campo 'Equipo de Venta' es obligatorio en pedidos
#. Habilitar el botón 'cancelar' incluso cuando el pedido está boqueado, requiere confirmación
#. Agregar busqueda por categoria en 'Equivalencia de puntos'
#. Agregar busqueda por producto en 'Equivalencia de puntos'
#. Campo 'Referencia Interna' se muestra en vista lista de Contactos
#. Seguimiento de cambios en 'Tarifa', 'Tipo', 'Equipo de Ventas' y 'Plazos de Pago'
#. Agregar busqueda por 'Nro de Documento' en filtro por defecto de Clientes
#. No copiar 'Referencia Interna' al duplicar un Partner
#. 'Referencia Interna' se posiciona antes que 'Dirección' en Partner
#. 'Fecha de pedido' mostrar siempre y cuando el pedido no esté cancelado

Ajustes a modulo Producto:

#. Permiso específico para ver costo de producto
#. 'Plazo de entrega del cliente' por defecto se define en 2 días
#. Agregar agrupamiento por 'Categoría de Producto' en listados de productos
#. Agregar busqueda por 'Referencia Interna' en listados de productos

Ajustes a modulo Contabilidad:

#. Seguimiento de cambios en Partner


Ajustes al modulo Inventario:

#. Agrupar por 'Equipo de Venta' en Productos y Plantillas de Productos
#. Agrupar por 'Equipo de Venta' en operaciones de Picking
#. Mostrar 'Referencia Interna' del Partner en reporte 'Operaciones de Albarán'
#. Ordenar por 'Ubicación' y 'Referencia Interna' del producto el reporte 'Operaciones de Albarán'
#. 'Fecha de caducidad' mostrar en vista lista de Lotes/Números de Serie
#. 'Fecha de Alerta' mostrar en vista lista de Lotes/Números de Serie
#. Campo 'Fecha de alerta' es obligatorio en Lotes/Números de Serie
#. Campo 'Fecha de caducidad' es obligatorio en Lotes/Números de Serie
#. Mostrar 'Bonificación' en vista lista de 'Categorías de Producto'


Módulo Compras

#. Habilitar el botón 'cancelar' incluso cuando la compra está boqueada, requiere confirmación
#. Habilitar el botón 'Crear Factura' incluso cuando la compra está bloqueada


Credits
=======

Contributors
------------

* Roberto Sierra <rsierra@ideadigital.com>

To contribute to this module, please visit https://www.gitlab.com/ayurdevas/

