# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models, fields


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    team_id = fields.Many2one(
        required=True,
        track_visibility='onchange',
    )

    pricelist_id = fields.Many2one(
        track_visibility='onchange',
    )

    payment_term_id = fields.Many2one(
        track_visibility='onchange',
    )

    type_id = fields.Many2one(
        track_visibility='onchange',
    )

    carrier_id = fields.Many2one(
        track_visibility='onchange',
    )
