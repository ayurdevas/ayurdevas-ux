# -*- coding: utf-8 -*-
# Copyright 2021 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import fields, models


class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    life_date = fields.Datetime(required=True)
    alert_date = fields.Datetime(required=True)
