# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    name = fields.Char(
        track_visibility='onchange',
    )

    ref = fields.Char(
        track_visibility='onchange',
        copy=False,
    )

    street = fields.Char(
        track_visibility='onchange',
    )

    street2 = fields.Char(
        track_visibility='onchange',
    )

    zip = fields.Char(
        track_visibility='onchange',
    )

    city = fields.Char(
        track_visibility='onchange',
    )

    state_id = fields.Many2one(
        track_visibility='onchange',
    )

    country_id = fields.Many2one(
        track_visibility='onchange',
    )

    partner_document_type_id = fields.Many2one(
        track_visibility='onchange',
    )

    vat = fields.Char(
        track_visibility='onchange',
    )

    property_account_position_id = fields.Many2one(
        track_visibility='onchange',
    )

    iibb_number = fields.Char(string="Numero IIBB")

    start_date = fields.Date(string="Fecha Inicio Actividad")

    email = fields.Char(
        track_visibility='onchange',
    )

    phone = fields.Char(
        track_visibility='onchange',
    )

    mobile = fields.Char(
        track_visibility='onchange',
    )

    customer = fields.Boolean(
        track_visibility='onchange',
    )

    property_payment_term_id = fields.Many2one(
        track_visibility='onchange',
    )

    property_product_pricelist = fields.Many2one(
        track_visibility='onchange',
    )

    supplier = fields.Boolean(
        track_visibility='onchange',
    )

    property_supplier_payment_term_id = fields.Many2one(
        track_visibility='onchange',
    )

    company_id = fields.Many2one(
        track_visibility='onchange',
    )

    sale_type = fields.Many2one(
        track_visibility='onchange',
    )

    sponsor_id = fields.Many2one(
        track_visibility='onchange',
    )

    date_start = fields.Date(
        track_visibility='onchange',
    )

    date_stop = fields.Date(
        track_visibility='onchange',
    )

    date_reincorporation = fields.Date(
        track_visibility='onchange',
    )

    leader_id = fields.Many2one(
        track_visibility='onchange',
    )

    team_id = fields.Many2one(
        track_visibility='onchange',
    )

    @api.multi
    def write(self, vals):
        if vals.get('name'):
            vals['name'] = str(vals['name']).strip().title()
        return super(ResPartner, self).write(vals)

    @api.multi
    def _handle_first_contact_creation(self):
        """ On creation of first contact for a company (or root) that has no address, assume contact address
        was meant to be company address

        Overriden original method to consider archived contacts
        Original method checks parent.child_ids == 1 but this is always zero
        because we create child addresses as archived
        """
        parent = self.parent_id
        address_fields = self._address_fields()
        if (parent.is_company or not parent.parent_id) and \
            any(self[f] for f in address_fields) and not any(parent[f] for f in address_fields):
            addr_vals = self._update_fields_values(address_fields)
            parent.update_address(addr_vals)
