# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    team_id = fields.Many2one(
        related='sale_id.team_id',
        string='Sales Team',
        readonly=True,
        store=True,
    )
